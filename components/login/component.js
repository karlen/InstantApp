﻿Components.Login = {

    background_color: '#4774A8',

    modules: [],

    Init: function (data, otherData) {


        setTimeout(function () {

            if (otherData.fromSplash) {
                $('.login_wrap .login_form input').fadeIn(300);
                $('.login_wrap .login_form .help_block').fadeIn(300);
            }
            $('.login_wrap .login_form .user_login').focus();

        }, 280);





        App.changeActivePage('login');


        $('.login_wrap .login_form').keypress(function (e) {
            if (e.which == 13) {

                Components.Login.LogIn();

            }
        });


        $('.login_wrap .login_form .submit_login').click(function () {

            Components.Login.LogIn();
        });


        $('.login_wrap .login_form .help_block .register').click(function () {

            Components.Login.Registration.Init();
        });


    },

    ShowLoader: function () {

        $('.login_wrap .login_form .submit_login').hide();

        $('.login_wrap .login_form .progress').show();

        $('.login_wrap .login_form .user_login').attr('readonly', 'readonly');

        $('.login_wrap .login_form .user_password').attr('readonly', 'readonly');


        return;


    },

    HideLoader: function () {


        $('.login_wrap .login_form .progress').hide();
        $('.login_wrap .login_form .submit_login').show();
        $('.login_wrap .login_form .user_login').removeAttr('readonly');
        $('.login_wrap .login_form .user_password').removeAttr('readonly');


        return;

    },

    LogIn: function () {

        if ($('.login_wrap .login_form .progress').css('display') == 'none') {

            this.ShowLoader();
            this.RequireFields();
        }

        return;
    },


    RequireFields: function () {

        if ($('.login_wrap .login_form .user_login').validate() == "#1" || $('.login_wrap .login_form .user_password').validate() == "#1") {


            Notification.alert('Пожалуйста заполните необходимые поля', function () {
                Components.Login.HideLoader();
                $('.login_wrap .login_form .user_login').focus();
            });


        }
        else if ($('.login_wrap .login_form .user_login').validate() == "#2" || $('.login_wrap .login_form .user_password').validate() == "#2") {

            Notification.alert('Некорректые данные', function () {
                Components.Login.HideLoader();
                $('.login_wrap .login_form .user_login').focus();

            });
        }
        else {
            this.CheckFields();
        }
    },

    CheckFields: function () {





        App.XHR(
            {
                do: 'login',
                login: $('.login_wrap .login_form .user_login').val(),
                password: $('.login_wrap .login_form .user_password').val()

            }, function (success_data) {

                if (success_data.data == "false") {
                    Notification.alert("Неверный логин или пароль", function () {
                        Components.Login.HideLoader();
                        $('.login_wrap .login_form .user_login').focus();
                    });
                }
                else {

                    Components.Login.GetUserData(success_data.data);
                }
            }, function () {
                Notification.alert("Проблема с сервером", function () {
                    Components.Login.HideLoader();
                    $('.login_wrap .login_form .user_login').focus();
                });

            });


    },

    GetUserData: function (user_data) {

        $.get(App.HostUrl + user_data['avatar'], function () {
            setTimeout(function () {

                $('.login_wrap .login_form input').hide();
                $('.login_wrap .login_form .help_block').hide();
                $('.login_wrap .login_form .progress').css('margin-top', '178px').show();
                $('.login_wrap .login_form .logo').addClass('rounded').css('background-image', 'url(' + App.HostUrl + user_data['avatar'] + ')');


                App.XHR(
                    {
                        do: 'getdata'

                    }, function (success_data) {

                        if (success_data.data == "false") {
                            Notification.alert("Проблема с сервером", function () {
                                App.loadComponent('login',{},{'fromSplash': true},function(){});

                            });
                        }
                        else {
                            App.loadComponent('main', {}, {data: success_data.data}, function () { });
                        }
                    }, function () {
                        Notification.alert("Проблема с сервером", function () {
                           App.loadComponent('login',{},{'fromSplash': true},function(){});
                        });

                    });


            }, 10);


        });


    },


    Registration: {

        Init: function () {

            this.openRegistrationPanel();

        },


        LoginAfterRegistration: function (login, password, success) {


            App.XHR(
                {
                    do: 'login',
                    login: login,
                    password: password

                }, function (success_data) {

                    if (success_data.data == "false") {
                        Notification.alert("Неверный логин или пароль", function () {
                            Components.Login.Registration.HideLoader();
                            $('.login_wrap .login_form .user_login').focus();
                            App.openRightPanel.hide_flyout();
                        });
                    }
                    else {

                        Components.Login.GetUserData(success_data.data);
                        success();
                    }
                }, function () {
                    Notification.alert("Проблема с сервером", function () {
                        Components.Login.Registration.HideLoader();
                        $('.login_wrap .login_form .user_login').focus();
                        App.openRightPanel.hide_flyout();

                    });

                });
        },

        ShowLoader: function () {

            App.openRightPanel.stopEvents();
            $('#right_panel > .main_flyout').hide();

        },
        HideLoader: function () {

            $('#right_panel > .main_flyout').show();
            App.openRightPanel.startEvents();


        },

        registerUser: function (fields) {


            Components.Login.Registration.ShowLoader();

            App.XHR(
                {
                    do: 'registration',
                    login: fields.login,
                    nickname: fields.nickname,
                    password: fields.password,
                    email: fields.email,
                    study: fields.study,
                    birthday: fields.birthdate.day,
                    birthmonth: fields.birthdate.month,
                    birthyear: fields.birthdate.year

                }, function (success_data) {

                    if (success_data.data == true) {


                        Components.Login.Registration.LoginAfterRegistration(fields.login, fields.password, function () {

                            App.openRightPanel.hide_flyout();
                        });

                    }
                    else {

                        if (success_data.data == '#1') {
                            Notification.alert("Логин \"" + fields.login + "\" уже занят!", function () {
                                Components.Login.Registration.HideLoader();
                                $('#right_panel > .main_flyout .registration .registration_wrap #login').addClass('error_val').focus();
                            });

                        }
                        else if (success_data.data == '#2') {
                            Notification.alert("Указанный e-mail уже зарегистрирован!", function () {
                                Components.Login.Registration.HideLoader();
                                $('#right_panel > .main_flyout .registration .registration_wrap #mail').addClass('error_val').focus();
                            });

                        }
                    }
                }, function () {
                    Notification.alert("Проблема с сервером", function () {
                        Components.Login.Registration.HideLoader();
                    });

                });


        },

        Register: function () {


            $('#right_panel > .main_flyout .registration .registration_wrap input').removeClass('error_val');


            var VRegExp = new RegExp(/^(\s|\u00A0)+/g);


            var login = $('#right_panel > .main_flyout .registration .registration_wrap #login').val();
            if (login.replace(VRegExp, '').length <= 1 || login.replace(VRegExp, '').length > 15) {
                $('#right_panel > .main_flyout .registration .registration_wrap #login').addClass('error_val').focus();
                return;
            }
            else if (!/^[a-zA-Z0-9]*$/.test(login)) {
                $('#right_panel > .main_flyout .registration .registration_wrap #login').addClass('error_val').focus();
                return;
            }


            var nickname = $('#right_panel > .main_flyout .registration .registration_wrap #nickname').val();
            if (nickname.replace(VRegExp, '').length <= 1) {
                $('#right_panel > .main_flyout .registration .registration_wrap #nickname').addClass('error_val').focus();
                return;
            }
            else if (!/^[a-zA-Z0-9а-яА-ЯЁё\u00A0 /g]*$/.test(nickname)) {
                $('#right_panel > .main_flyout .registration .registration_wrap #nickname').addClass('error_val').focus();
                return;
            }


            var password = $('#right_panel > .main_flyout .registration .registration_wrap #password').val();
            if (password.replace(VRegExp, '').length < 6) {
                $('#right_panel > .main_flyout .registration .registration_wrap #password').addClass('error_val').focus();
                return;
            }


            var re_password = $('#right_panel > .main_flyout .registration .registration_wrap #re_password').val();

            if (re_password.replace(VRegExp, '') != password.replace(VRegExp, '')) {
                $('#right_panel > .main_flyout .registration .registration_wrap #re_password').addClass('error_val').focus();
                return;
            }


            var email = $('#right_panel > .main_flyout .registration .registration_wrap #mail').val();
            if (!/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(email)) {
                $('#right_panel > .main_flyout .registration .registration_wrap #mail').addClass('error_val').focus();
                return;
            }


            var calendar = document.querySelector('#right_panel > .main_flyout .registration .registration_wrap #birthdate').winControl;

            var birthdate = {
                year: calendar.current.getUTCFullYear(),
                month: calendar.current.getUTCMonth() + 1,
                day: calendar.current.getUTCDate()
            };


            var study = $('#right_panel > .main_flyout .registration .registration_wrap #study').val();


            var fields = {
                login: login,
                nickname: nickname,
                password: password,
                email: email,
                study: study,
                birthdate: birthdate

            };


            Components.Login.Registration.registerUser(fields);


        },

        openRegistrationPanel: function () {


            App.openRightPanel('registration', '.registration > .registration_header > .registration_close .registration_backbutton', {}, {}, function () {

                WinJS.UI.processAll();
                $("#right_panel > .main_flyout .registration_wrap input:first").focus();


                $('#right_panel > .main_flyout > .registration').keypress(function (e) {
                    if (e.which == 13) {

                        Components.Login.Registration.Register();

                    }
                });

                $("#right_panel > .main_flyout > .registration >  #registration_button").on("click", function () {


                    Components.Login.Registration.Register();

                });


            });


        }

    },


};
