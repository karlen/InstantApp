Components.Main = {

    background_color: '#EDF2F7',

    modules: ['my_profile', 'news', 'comments', 'messages', 'friends', 'groups', 'photos', 'videos', 'audios', 'favorites', 'search', 'settings'],

    Init: function (data, otherData) {

       var splitView = document.querySelector(".splitView").winControl;

        this.LoadModule(this.modules[0]);

        $('.app_container .win-splitview .nav-commands .command[data-module]').on('click', function () {
            splitView.closePane();
            Components.Main.LoadModule($(this).attr('data-module'));
        });

        $(".app_container .win-splitview-contentwrapper .main_app_page .module_main_box").mousewheel(function (event, delta) {

            var sc = $('.app_container .win-splitview-contentwrapper .main_app_page .module_main_box').scrollLeft();
            sc -= (delta * 15);

            $('.app_container .win-splitview-contentwrapper .main_app_page .module_main_box').scrollLeft(sc);

            event.preventDefault();

        });


        $('.app_container .win-splitview-contentwrapper .main_app_page .top_nav').on('click', function () {
            $('.app_container .win-splitview-contentwrapper .main_app_page .module_main_box').animate({
                scrollLeft: 0
            }, 400);
        });


    },

    ShowAppLoader: function (module_loader) {
        var sprinter = $('.app_container .win-splitview-contentwrapper .app_module_loader > .spinner').detach();

        sprinter.appendTo('.app_container .win-splitview-contentwrapper .app_module_loader');
        $('.app_container .win-splitview-contentwrapper .app_module_loader').css('visibility', 'visible');
        $('.app_container .win-splitview-contentwrapper .app_module_loader > .spinner').show();
        if (module_loader) {
            $('.app_container .win-splitview-contentwrapper .main_app_page > .module_main_box').css('visibility', 'hidden');
        }

    },

    HideAppLoader: function (module_loader) {
        $('.app_container .win-splitview-contentwrapper .app_module_loader').css('visibility', 'hidden');
        $('.app_container .win-splitview-contentwrapper .app_module_loader > .spinner').hide();
        if (module_loader) {
            $('.app_container .win-splitview-contentwrapper .main_app_page > .module_main_box').css('visibility', 'visible');
            App.Animation.Init(document.querySelector('.app_container .win-splitview-contentwrapper .main_app_page .module_main_box'));
        }


    },

    LoadModule: function (module_name) {

        var object = $('.app_container .win-splitview .nav-commands .command[data-module="' + module_name + '"]');
        if ($(object).hasClass('active_comand')) {
            return;
        }


        this.ShowAppLoader(true);

        var title = $('.command-title > span', object).html();
        var element = document.querySelector(".app_container .win-splitview-contentwrapper .main_app_page .top_nav");
        $('h3', element).html(title);
        $('.app_container .win-splitview .nav-commands .command[data-module]').removeClass('active_comand');
        $(object).addClass('active_comand');
        WinJS.UI.Animation.enterPage(element, {top: "0px", left: "75px", rtlflip: true});


        App.loadModule(module_name, '.app_container .win-splitview-contentwrapper .main_app_page > .module_main_box', {}, {}, function () {


                            Components.Main.HideAppLoader(true);

        });


    }


};