Modules.My_profile = {

    widgets: ['profile_main', 'profile_info', 'profile_data', 'profile_wall'],

    Init: function (data, otherData, success) {

        App.XHR(
            {
                do: 'getuserdata'

            }, function (success_data) {

                if (success_data.data == "false") {

                }
                else {

                    Modules.My_profile.loadWidgets(success_data.data, success);

                }
            }, function () {
                Notification.alert("Проблема с сервером", function () {

                });

            });

    },


    loadWidgets: function (data, success) {

        var widgets = [
            {
                widget_name: 'profile_main',
                position: '.app_container .win-splitview-contentwrapper .main_app_page > .module_main_box > .profile_main',
                type:'html',
                TplData: {},
                OtherData: {avatar: data.avatar, karma: data.karma, rating: data.rating},
                success: function () {

                }
            },
            {
                widget_name: 'profile_info',
                position: '.app_container .win-splitview-contentwrapper .main_app_page > .module_main_box > .profile_info',
                type:'html',
                TplData: {},
                OtherData: {
                    birthdate: data.birthdate,
                    city: data.city,
                    description: data.description,
                    email: data.email,
                    gender: data.gender,
                    icq: data.icq,
                    phone: data.phone,
                    regdate: data.regdate,
                    status: data.status
                },
                success: function () {

                }
            },
            {
                widget_name: 'profile_data',
                position: '.app_container .win-splitview-contentwrapper .main_app_page > .module_main_box > .profile_data',
                type:'html',
                TplData: {},
                OtherData: {friends:data.friends,clubs:data.clubs,albums:data.albums},
                success: function () {

                }
            },
            {
                widget_name: 'profile_wall',
                position: '.app_container .win-splitview-contentwrapper .main_app_page > .module_main_box',
                type: 'append',
                TplData: {},
                OtherData: {smilies:App.Smilies,wall:data.wall},
                success: function () {

                }
            }
        ];


        App.loadWidgets(0, widgets, function () {
            success();
        });


    }


}