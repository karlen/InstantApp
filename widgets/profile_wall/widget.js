Widgets.Profile_wall = {


    Init:function(){


        this.loadSmilies();




    },

    loadSmilies:function(){

        var smiles = App.Smilies;
        var count = Math.ceil(Object.keys(smiles).length / 2);
        var swdth = 50 * count;


        $('.app_container .win-splitview-contentwrapper .main_app_page > .module_main_box > .profile_wall .smilies .smilieslist').css('width', swdth + 'px');


        $(".app_container .win-splitview-contentwrapper .main_app_page > .module_main_box > .profile_wall .smilies").mousewheel(function (event, delta) {

            var sc = $('.app_container .win-splitview-contentwrapper .main_app_page > .module_main_box > .profile_wall .smilies').scrollLeft();
            sc -= (delta * 15);

            $('.app_container .win-splitview-contentwrapper .main_app_page > .module_main_box > .profile_wall .smilies').scrollLeft(sc);

            event.stopPropagation();

        });


        $('.app_container .win-splitview-contentwrapper .main_app_page > .module_main_box > .profile_wall .smilies .smile_it').each(function(){

            var url = $(this).attr('data-url');
            $(this).css('background-image','url("'+url+'")');

        });



        $('.app_container .win-splitview-contentwrapper .main_app_page > .module_main_box > .profile_wall .wall_add_content').on('keydown keyup input  DOMNodeInserted DOMNodeRemoved', function () {

            if (!$(this).text()) {
                $(this).attr('data-placeholder', 'Добавить запись на стену...');
            }
            else {
                $(this).removeAttr('data-placeholder');

            }
        });

    }


};

