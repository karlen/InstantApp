Components = {};

Modules = {};

Widgets = {};

User = {};

Updater = {};

Storage = {};

PopUp = {};

Navigation = {};

Camera = {};

Notification = {

    isAlertShowing: false,
    alertStack: [],


    alert: function (message, success) {

        if (this.isAlertShowing) {
            var later = function () {
                Notification.alert(message, success);
            };
            this.alertStack.push(later);
            return;
        }
        this.isAlertShowing = true;


        App.loadTemplate('includes/templates/alert.html', {}, {'message': message}, '.app_container', 'append', function () {


            $('body > .app_container .main_alert_box .alert_box .alert_content .alert_close_button').click(function () {


                $('body > .app_container .main_alert_box').remove();
                success();
                Notification.isAlertShowing = false;


                if (Notification.alertStack.length) {
                    setTimeout(Notification.alertStack.shift(), 0);
                }


            });


        });


    },


    beep: function (onEvent) {


        var audio = document.getElementsByTagName('audio')[0];


        audio.play();

        onEvent();


    }


};

Tools = {};

App = {

    ActivePage: null,

    Smilies: [
        {'name': 'angel'},
        {'name': 'crazy'},
        {'name': 'cry'},
        {'name': 'dance'},
        {'name': 'glasses'},
        {'name': 'hoho'},
        {'name': 'joke'},
        {'name': 'laugh'},
        {'name': 'look'},
        {'name': 'love'},
        {'name': 'music'},
        {'name': 'rofl'},
        {'name': 'sad'},
        {'name': 'scratch'},
        {'name': 'shock'},
        {'name': 'sick'},
        {'name': 'smile'},
        {'name': 'smoke'},
        {'name': 'stuk'},
        {'name': 'v'},
        {'name': 'zlo'},
        {'name': 'zst'}
    ],

    HostUrl: 'http://instantchat.ru',


    Animation: {

        Init: function (DOMelement) {


            var winAnimation = {
                left: [],
                top: []
            };

            $('[data-animation]', DOMelement).each(function () {

                if ($(this).attr('data-animation') == 'win-left') {
                    winAnimation.left.push(this);
                }
                else if ($(this).attr('data-animation') == 'win-top') {
                    winAnimation.top.push(this);
                }


            });


            WinJS.UI.Animation.enterPage(winAnimation.left, {top: "0px", left: "75px", rtlflip: true});
            WinJS.UI.Animation.enterPage(winAnimation.top, {top: "35px", left: "0px", rtlflip: true});


        }

    },

    InitPage: function () {
        $.views.helpers({
            HostUrl: App.HostUrl

        });

        //App.loadComponent('main', {}, {'fromSplash': true,data:{
        //    'nickname':"Karlen Avetisyan",avatar:'images/users/avatars/8d97dc0cf5d62759b247ffb5f3a8b3f9.jpg',in_messages:{messages:1,notifications:1,total:2}
        //}}, function () {
        //
        //    Loader.HideSplashScreen($('.splash_screen'));
        //
        //});

        App.loadComponent('login', {}, {'fromSplash': true}, function () {


            Loader.HideSplashScreen($('.splash_screen'));


        });
    },

    makeTmpId: function () {

        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    },

    XHR: function (data, success, fail) {


        data = Object.keys(data).map(function (k) {
            return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
        }).join('&');


        WinJS.xhr({
            url: App.HostUrl + '/messenger',
            type: 'POST',
            data: data,
            headers: {
                "content-type": "application/x-www-form-urlencoded"
            }
        }).done(
            function completed(request) {

                if (request.status === 200) {

                    try {

                        var req_data = JSON.parse(request.responseText);
                        success(req_data);


                    } catch (err) {

                        success({data: 'false'});

                    }


                }
                else {


                    setTimeout(function () {

                        WinJS.xhr({
                            url: App.HostUrl + '/messenger',
                            type: 'POST',
                            data: data,
                            headers: {
                                "content-type": "application/x-www-form-urlencoded"
                            }
                        }).done(
                            function completed(request_repeat) {
                                if (request_repeat.status === 200) {


                                    try {

                                        var req_data = JSON.parse(request_repeat.responseText);
                                        success(req_data);


                                    } catch (err) {

                                        success({data: 'false'});

                                    }


                                }
                                else {
                                    fail();
                                }
                            },
                            function error(request) {
                                fail();
                            }
                        );


                    }, 7000);


                }
            },
            function error(request) {
                setTimeout(function () {

                    WinJS.xhr({
                        url: App.HostUrl + '/messenger',
                        type: 'POST',
                        data: data,
                        headers: {
                            "content-type": "application/x-www-form-urlencoded"
                        }
                    }).done(
                        function completed(request_repeat) {
                            if (request_repeat.status === 200) {

                                try {

                                    var req_data = JSON.parse(request_repeat.responseText);
                                    success(req_data);


                                } catch (err) {

                                    success({data: 'false'});

                                }
                            }
                            else {
                                fail();
                            }
                        },
                        function error(request) {
                            fail();
                        }
                    );


                }, 7000);
            }
        );


    },

    changeActivePage: function (page) {
        App.ActivePage = page;
        //Storage.add('activePage', page);
    },

    loadTemplate: function (url, data, other_data, position, type, success) {

        $.get(url, function (Htmldata) {


            template_id = 'new_' + App.makeTmpId();


            var script = document.createElement("script");
            script.innerHTML = Htmldata;
            script.setAttribute("id", template_id);
            script.setAttribute("type", "text/x-jsrender");
            document.head.appendChild(script);


            if (type == 'html') {
                $(position).html(
                    $("#" + template_id).render(data, other_data)
                );
            }
            else if (type == 'append') {
                $(position).append(
                    $("#" + template_id).render(data, other_data)
                );
            }
            else if (type == 'prepend') {

                $(position).prepend(
                    $("#" + template_id).render(data, other_data)
                );
            }


            $('#' + template_id).remove();

            success();

        });


    },

    loadComponent: function (component, Tpldata, TplOtherData, success) {


        Components = {};
        Modules = {};
        Widgets = {};

        $('.app_container').css('visibility', 'hidden');
        var componentname = component,
            componet_var = 'Components.' + componentname.charAt(0).toUpperCase() + componentname.slice(1),
            position = '.app_container',
            style = document.createElement("link");


        style.setAttribute("rel", "stylesheet");
        style.setAttribute("href", "components/" + componentname + "/css/component.css");
        style.setAttribute("data-added", "1");
        document.head.appendChild(style);


        App.loadTemplate("components/" + componentname + "/templates/component.html", Tpldata, TplOtherData, position, 'html', function () {


            var scriptElem = document.createElement('script');
            scriptElem.setAttribute('src', "components/" + componentname + "/component.js");
            scriptElem.setAttribute('data-component', componentname);
            scriptElem.onload = function () {

                var Component_obj = eval(componet_var);


                $('body').css('background-color', Component_obj.background_color);


                setTimeout(function () {

                    $('[data-animation]').each(function () {


                        $(this).addClass($(this).attr('data-animation'));

                    });


                    $('[data-scale = true]').scaleAble();

                    setTimeout(function () {
                        success();

                        WinJS.UI.processAll().done(function () {
                            $('.app_container').css('visibility', 'visible');
                            Component_obj.Init(Tpldata, TplOtherData);
                        });


                    }, 1);

                }, 10);


            };
            if (document.body) {
                document.body.appendChild(scriptElem);
            } else {
                document.head.appendChild(scriptElem);
            }


            $('script[data-component=' + componentname + ']').remove();


        });

    },

    loadModule: function (module, position, Tpldata, TplOtherData, success) {

        Modules = {};
        Widgets = {};


        var modulename = module,
            module_var = 'Modules.' + modulename.charAt(0).toUpperCase() + modulename.slice(1),
            style = document.createElement("link");


        style.setAttribute("rel", "stylesheet");
        style.setAttribute("href", "modules/" + modulename + "/css/module.css");
        style.setAttribute("data-added", "1");
        document.head.appendChild(style);

        $(position).html(' ');
        App.loadTemplate("modules/" + modulename + "/templates/module.html", Tpldata, TplOtherData, position, 'html', function () {


            var scriptElem = document.createElement('script');
            scriptElem.setAttribute('src', "modules/" + modulename + "/module.js");
            scriptElem.setAttribute('data-component', modulename);
            scriptElem.onload = function () {
                var Module_obj = eval(module_var);

                setTimeout(function () {

                    $('[data-animation]').each(function () {


                        $(this).addClass($(this).attr('data-animation'));

                    });


                    $('[data-scale = true]').scaleAble();

                    setTimeout(function () {

                        WinJS.UI.processAll().done(function () {
                            Module_obj.Init(Tpldata, TplOtherData, success);
                        });


                    }, 1);

                }, 10);


            };
            if (document.body) {
                document.body.appendChild(scriptElem);
            } else {
                document.head.appendChild(scriptElem);
            }


            $('script[data-component=' + modulename + ']').remove();

        });


    },


    loadWidget: function (widgetname, position, type, Tpldata, TplOtherData, success) {

        style = document.createElement("link");

        style.setAttribute("rel", "stylesheet");
        style.setAttribute("href", "widgets/" + widgetname + "/css/widget.css");
        style.setAttribute("data-added", "1");
        document.head.appendChild(style);


        App.loadTemplate("widgets/" + widgetname + "/templates/widget.html", Tpldata, TplOtherData, position, type, function () {


            var widget_var = 'Widgets.' + widgetname.charAt(0).toUpperCase() + widgetname.slice(1);


            var scriptElem = document.createElement('script');
            scriptElem.setAttribute('src', "widgets/" + widgetname + "/widget.js");
            scriptElem.setAttribute('data-widget', widgetname);
            scriptElem.onload = function () {
                var Widget_obj = eval(widget_var);

                WinJS.UI.processAll().then(function () {
                    $('[data-animation]').each(function () {


                        $(this).addClass($(this).attr('data-animation'));

                    });


                    $('[data-scale = true]').scaleAble();
                    Widget_obj.Init(Tpldata, TplOtherData);
                    success();

                });


            };
            if (document.body) {
                document.body.appendChild(scriptElem);
            } else {
                document.head.appendChild(scriptElem);
            }


            $('script[data-widget=' + widgetname + ']').remove();


        });
    },

    loadWidgets: function (i, widgets, success) {

        var count = Object.keys(widgets).length - 1;

        var widget = widgets[i];


        if (i < count) {

            setTimeout(function () {

                App.loadWidget(widget.widget_name, widget.position, widget.type, widget.TplData, widget.OtherData, function () {

                    App.loadWidgets(i + 1, widgets, success);

                });
            }, 100);
        }
        else {

            setTimeout(function () {
                App.loadWidget(widget.widget_name, widget.position, widget.type, widget.TplData, widget.OtherData, function () {

                    success();

                });
            }, 100);

        }


    },

    openRightPanel: function (template, close_button, Tpldata, TplOtherData, success) {

        var prototype = Object.getPrototypeOf(App.openRightPanel);
        prototype.flyout_control = null;
        prototype.stopEvents = function () {

            $('.win-clickeater').off("click");


        };
        prototype.startEvents = function () {

            $('.win-clickeater').on('click', function () {

                App.openRightPanel.flyout_control.hide();


            });

        };

        prototype.hide_flyout = function () {


            App.openRightPanel.flyout_control.hide();


        };


        var parent = $('.win-settingsflyout').parent().get(0);

        if (parent) {

            if ($('#right_panel > .main_flyout').attr('data-template') != template) {
                parent.remove();
            }

        }


        WinJS.UI.SettingsFlyout.showSettings("right_panel", "includes/templates/right_panel.html", function (flyout_control) {


            App.openRightPanel.flyout_control = flyout_control;

            App.loadTemplate('includes/templates/' + template + '.html', Tpldata, TplOtherData, '#right_panel > .main_flyout', 'html', function () {

                $('#right_panel > .main_flyout').attr('data-template', template);
                setTimeout(function () {
                    $('#right_panel > .main_flyout').css('visibility', 'visible');


                    WinJS.UI.processAll().then(function () {
                        success();
                        App.Animation.Init(document.querySelector('#right_panel > .main_flyout'));
                    });


                    $('#right_panel > .main_flyout > ' + close_button).on('click', function () {

                        flyout_control.hide();


                    });


                    $('.win-clickeater').on('click', function () {

                        flyout_control.hide();


                    });

                }, 1000);


            });


        }, function () {


            $('#right_panel > .main_flyout input')[0].focus();

        });


    },


};


$.fn.extend({
    scaleAble: function () {


        $(this).on('mousedown', function () {
            if (!$(this).hasClass('active')) {

                $(this).addClass('active_scale');

            }
        }).on('mouseup', function () {
            if (!$(this).hasClass('active')) {
                $(this).removeClass('active_scale');
            }
        }).hover(function () {
        }, function () {
            if (!$(this).hasClass('active')) {
                $(this).removeClass('active_scale');
            }
        });


    },
    ismail: function () {

        var value = $(this).val();
        var VRegExp = new RegExp(/^(\s|\u00A0)+/g);

        filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (filter.test(value)) {
            return true;
        }
        else {
            return false;
        }


    },
    validate: function () {

        var value = $(this).val();
        var VRegExp = new RegExp(/^(\s|\u00A0)+/g);
        if (value) {
            value = value.replace(VRegExp, '');
            if (value.length <= 0) {
                return "#1";
            }
            else if (value.length <= 4) {
                return "#2";
            }
            else {
                return true;
            }
        }
        else {
            return "#1";
        }


    }

});

