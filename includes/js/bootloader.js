Loader = {

    Links: [
        {url: "includes/winjs/css/ui-light.css", type: "link"},
        {url: "includes/css/index.css", type: "link"},
        {url: "includes/js/jquery.js", type: "script"},
        {url: "includes/js/jsrender.min.js", type: "script"},
        {url: "includes/js/jquery.mousewheel.min.js", type: "script"},
        {url: "includes/js/core.js", type: "script"}

    ],


    LoadFrameWork: function (success) {

        var DomElem = document.createElement("script");
        DomElem.type = 'text/javascript';
        DomElem.async = 'async';
        DomElem.src = "includes/winjs/js/base.min.js";

        DomElem.onload = function () {
            var DomElem = document.createElement("script");
            DomElem.type = 'text/javascript';
            DomElem.async = 'async';
            DomElem.src = "includes/winjs/js/ui.js";
            DomElem.onload = function () {

                setTimeout(function(){
                document.querySelector('.splash_screen').style.backgroundImage = 'url(images/splash.svg), url(images/windows_loader.gif)'
                success();
                },100);

            };
            document.head.appendChild(DomElem);
        };

        document.head.appendChild(DomElem);



    },

    ShowSplashScreen: function () {


        var Elem = document.createElement('div');
        Elem.className = "splash_screen";
        Elem.innerHTML = ' ';
        document.body.appendChild(Elem);


        this.LoadFrameWork(function(){


        setTimeout(function () {


            Loader.LoadLinks(function () {

                App.InitPage();


            });


        }, 1500);
        });


    },

    HideSplashScreen: function (elem) {

        document.getElementsByTagName('style')[0].remove();
        elem.remove();
        document.getElementById('splash_script').remove();
        delete Loader;


    },

    LoadLinks: function (success) {


        var elem = Loader.Links;
        var endCount = Object.keys(elem).length - 1;

        function loadelem(i) {


            var type = elem[i].type;

            if (type == "script") {
                var DomElem = document.createElement("script");
                DomElem.type = 'text/javascript';
                DomElem.async = 'async';
                DomElem.src = elem[i].url;
            }
            else {
                var DomElem = document.createElement("link");
                DomElem.rel = 'stylesheet';
                DomElem.type = 'text/css';
                DomElem.async = 'true';
                DomElem.href = elem[i].url;
            }


            if (i < endCount) {

                DomElem.onload = function () {

                    setTimeout(function () {
                        loadelem(i + 1);
                    }, 10);


                };
            }
            else if (i == endCount) {
                DomElem.onload = function () {
                    setTimeout(function () {
                        success();
                    }, 1500);
                };
            }

            document.head.appendChild(DomElem);

        }


        loadelem(0);


    }

};


Loader.ShowSplashScreen();